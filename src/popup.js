mdc.textField.MDCTextField.attachTo(document.querySelector('.mdc-text-field'));
mdc.checkbox.MDCCheckbox.attachTo(document.querySelector('.mdc-checkbox'));

var labelNotice = document.getElementById('label-notice');
var inputTime = document.getElementById('input-time');
var buttonSubmint = document.getElementById('button-submit');
var checkbox = document.getElementById('checkbox');

//页面打开时向后台查询运行状态
chrome.runtime.sendMessage({
	type: "status"
}, function(response) {
	refreshStatus(response);
});

buttonSubmint.onclick = function() {
	var time = inputTime.value;
	var refresh = checkbox.checked;
	chrome.runtime.sendMessage({
		type: "screenshot",
		time: time,
		refresh: refresh
	}, function(response) {
		refreshStatus(response);
	});
}

//刷新页面数据
function refreshStatus(response) {
	labelNotice.innerHTML = "运行状态：" + response.status + "，间隔时长：" + response.time;
	checkbox.checked = response.refresh
	inputTime.focus();
	inputTime.value = response.time;
}
